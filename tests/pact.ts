import {Pact} from "@pact-foundation/pact";
import path =require("path");

export namespace Contract {
    let pact: Pact;
    let counter: number;

    export function createPact(): void {
        counter = 0;
        pact = new Pact({
            // if not specified port is specified automatically,
            log: path.resolve(process.cwd(), "pact/logs", "mockserver-integration.log"),
            dir: path.resolve(process.cwd(), "pact/pacts"),
            spec: 2,
            consumer: "DemoConsumer",
            provider: "DemoProvider",
        });
    }

    export function getPact(): Pact {
        counter += 1;
        return pact;
    }

    export function closePact(): void {
        counter -= 1;
        if(counter <= 0) {
            pact.finalize();
        }
    }
}
