import * as chai from "chai";
import chaiAsPromised = require("chai-as-promised");
import sinonChai = require("sinon-chai");
import {Interaction, Matchers, Pact} from "@pact-foundation/pact";
import {CustomerClient} from "../src/clients/customerClient";
import {Contract} from "./pact";
import {like} from "@pact-foundation/pact/dsl/matchers";

const {Config} = require("../src/config");
const expect = chai.expect;

const {eachLike} = Matchers;

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe("The Customer API", () => {
    // load config
    if (Config.loadConfig()) {
        process.exit();
    }

    const url = Config.instance.url;
    let customerClient: CustomerClient;

    const provider = Contract.getPact();

    const customerExample = {
        "id": "1",
        "firstName": "Max",
        "lastName": "Mustermann",
        "email": "noreply@mustermannmax.com"
    }

    before(() =>
        provider.setup().then(opts => {
            customerClient = new CustomerClient({url, port: opts.port});
        })
    );

    after(() => Contract.closePact());

    afterEach(() => provider.verify());

    describe("get /customers using builder pattern", () => {
        const EXPECTED_BODY = eachLike(customerExample)
        before(() => {
            const interaction = new Interaction()
                .given("I have a list of customers")
                .uponReceiving("a request for all customers")
                .withRequest({
                    method: "GET",
                    path: "/customers",
                    headers: {
                        Accept: "application/json",
                    },
                })
                .willRespondWith({
                    status: 200,
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    body: EXPECTED_BODY,
                });

            return provider.addInteraction(interaction);
        });

        it("returns the correct response", done => {
            customerClient.getCustomers()
                .then((response: any) => {
                    expect(response.data[0]).to.deep.eq(customerExample)
                    done()
                }, done)
        });
    });

    describe("post /customer using builder pattern", () => {
        const EXPECTED_BODY = {
            "status": "success",
            "customerId": "1"
        };

        before(() => {
            const interaction = new Interaction()
                .given("Init state")
                .uponReceiving("a request to create a customer with the builder pattern")
                .withRequest({
                    method: "POST",
                    path: "/customer",
                    body: like(customerExample),
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    }
                })
                .willRespondWith({
                    status: 200,
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    body: like(EXPECTED_BODY)
                });

            return provider.addInteraction(interaction);
        });

        it("returns the correct response", done => {
            customerClient.postCustomer(customerExample)
                .then((response: any) => {
                    expect(response.data).to.deep.eq(EXPECTED_BODY)
                    done()
                }, done)
        });
    });

});