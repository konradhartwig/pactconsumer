import wrapper from "@pact-foundation/pact-node";
import {Contract} from "./pact";

// create pact
Contract.createPact();

// used to kill any left over mock server instances
process.on("SIGINT", () => {
    wrapper.removeAllServers();
});
