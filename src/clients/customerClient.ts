import axios, {AxiosPromise} from "axios";

export class CustomerClient {
    private url: string;
    private port: string;

    constructor(endpoint: any) {
        this.url = endpoint.url;
        this.port = endpoint.port;
    }
    public getCustomers = () : AxiosPromise => {
        return axios.request({
            baseURL: `${this.url}:${this.port}`,
            headers: { Accept: "application/json" },
            method: "GET",
            url: "/customers",
        })
    }

    public postCustomer = (data) : AxiosPromise => {
        return axios.request({
            baseURL: `${this.url}:${this.port}`,
            headers: { "Content-Type": "application/json; charset=utf-8" },
            method: "POST",
            url: "/customer",
            data: data
        });
    }
}