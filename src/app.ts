import express from 'express';
import bodyParser from "body-parser";
import {Config} from "./config";
import CustomerHandler from "./handlers/customerHandler";
import HealthHandler from "./handlers/healthHandler";
import * as utils from "./utils"

export class CustomerServer {
    private readonly _server: express.Application;

    constructor() {
        // create express server
        this._server = express();

        // load Config
        if(Config.loadConfig()){
            process.exit();
        }

        // use parser
        this._server.use(bodyParser.json());
    }

    public start(): void {
        // connect database
        utils.connectToDB();

        // use handlers
        this._server.use(new CustomerHandler().getRouter());
        this._server.use(new HealthHandler().getRouter());

        // listen for http requests
        this._server.listen(Config.instance.port, err => {
            if (err) {
                return console.error(err);
            }
            return console.log(`Server is listening on port ${Config.instance.port}`);
        });
    }
}

const app = new CustomerServer();
app.start();
