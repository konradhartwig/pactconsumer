export namespace Config {
    export let instance: ConfigHolder;

    function getValue(value: any, name: string, status: [boolean]): any {
        if(!value) {
            console.error(`Missing ${name} in environment definition`);
            status[0] = true;
        }

        return value;
    }

    export function loadConfig(): boolean {
        let status: [boolean] = [false];
        instance = new ConfigHolder(status);
        return status[0];
    }

    class ConfigHolder {
        public readonly port: number;
        public readonly mongoUri: string;
        public readonly url: string;

        constructor(status: [boolean]) {
            this.port = getValue(process.env.PORT, "PORT", status);
            this.mongoUri = getValue(process.env.MONGOURI, "MONGOURI", status);
            this.url = getValue(process.env.URL, "URL", status);
        }
    }
}