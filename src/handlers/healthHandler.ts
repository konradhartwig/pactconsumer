import Handler from "./handler";
import express from "express";
import * as utils from "../utils";

export default class HealthHandler extends Handler {
    constructor() {
        super();
        this.getRouter().get("/health", this.getHealth.bind(this));
    }

    private async getHealth(req: express.Request, res: express.Response): Promise<void> {
        try {
            res.send("Server is up and running!");
        } catch (e) {
            utils.handleError(e.message, res);
        }
    }
}