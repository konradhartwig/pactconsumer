# Pact Consumer Test-Server

## Pact Broker
-   Host ![pact broker](https://github.com/pact-foundation/pact_broker) or use ![pactflow.io](https://pactflow.io/?utm_source=github&utm_campaign=pact_broker_intro)
-   Get pact broker Read/write token and pact broker base url
-   Set CI/CD variables
-   Create pipeline trigger
-   Create webhook at pact broker to trigger provider pipeline (Insert webhook provided by gitlab CI/CD)
    -   Example body:
```
    {
        "description": "Trigger provider verification",
        "provider": { "name": "MyProvider" },
        "enabled": true,
        "request": {
            "method": "POST",
            "url": "https://gitlab.com/api/v4/projects/1122344/ref/master/trigger/pipeline?token=12345678&variables[PACT_CONSUMER_TAG]=${pactbroker.consumerVersionTags}",
            "body": ""
        },
        "events": [{ "name": "contract_content_changed" }]
    }
```  

## API
-   GET /customers  
-   POST /customer 
    -   Example body:
```
    {
        "id": "1",
        "firstName": "Max",
        "lastName": "Mustermann",
        "email": "noreply@maxmustermann.com"
    }   
```      

## Example .env
```
PORT=4000
URL=http://localhost
MONGOURI=mongodb://127.0.0.1:27017/local
```

## Setup
##### Initialize dependencies:  
```  
yarn install  
```  
##### Start db docker:  
```  
docker run -d -p 27017:27017 mongo  
```  
##### Run pact consumer tests:  
```  
yarn test  
```  
##### Run pact provider tests:  
```  
yarn verify  
```  